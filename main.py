class BankAccount:
    def __init__(self, account_number, account_holder, balance):
        self._account_number = account_number
        self._account_holder = account_holder
        self._balance = balance

    def deposit(self, amount):
        self._balance += amount

    def withdraw(self, amount):
        if (self._balance - amount) < 0:
            raise ValueError("Insufficient funds on the balance sheet")
        else:
            self._balance -= amount

    def get_balance(self):
        return self._balance

    def display_account_info(self):
        print(self._account_number, self._account_holder, self._balance)



